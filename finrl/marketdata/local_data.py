import pandas as pd
from finrl.preprocessing.data import data_split


class DataQuery:
    """
    API for query data from online and local
    """
    def __init__(self, start_date: str, end_date: str):
        self.start_date = start_date
        self.end_date = end_date
        
    def fetch_data(self) -> pd.DataFrame:
        pass
    
    def select_equal_rows_stock(self, df):
        pass
        

class LocalQuery(DataQuery):
    """
    API for query data from local
    """
    def __init__(self, start_date: str, end_date: str, files: list):
        super().__init__(start_date, end_date)
        self.start_date = self.start_date.replace('/', '-')
        self.end_date = self.end_date.replace('/', '-')
        self.files = files

    def fetch_data(self) -> pd.DataFrame:
        data_df = pd.DataFrame()
        name_file = [i.split('/')[-1][:-4] for i in self.files]
        for name, file in zip(name_file, self.files):
            tmp_df = pd.read_csv(file)
            tmp_df.columns = tmp_df.columns.str.lower()
            tmp_df['tic'] = name
            data_df = data_df.append(tmp_df)
        data_df.reset_index()
        try:
            col = [
                "date",
                "open",
                "high",
                "low",
                "close",
                "volume",
                "tic",
            ]
            data_df = data_df[col]
        except:
            print("the features are not supported currently")
        # create day of the week column (monday = 0)
        data_df['date'] = pd.to_datetime(data_df['date'], format="%Y/%m/%d")
        # print(data_df['date'])
        data_df["day"] = data_df["date"].dt.dayofweek
        # convert date to standard string format, easy to filter
        data_df["date"] = data_df.date.apply(lambda x: x.strftime("%Y-%m-%d"))
        # drop missing data
        data_df = data_df.dropna()
        data_df = data_df.reset_index(drop=True)
        print("Shape of DataFrame: ", data_df.shape)

        data_df = data_df.sort_values(by=['date','tic']).reset_index(drop=True)
        data_df = data_split(data_df, self.start_date, self.end_date)
        return data_df
    
    def select_equal_rows_stock(self, df):
        super().select_equal_rows_stock(df)

